# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=jql
pkgver=5.1.6
pkgrel=0
pkgdesc="A JSON Query Language CLI tool"
url="https://github.com/yamafaktory/jql"
arch="all"
license="MIT"
makedepends="cargo"
source="https://github.com/yamafaktory/jql/archive/v$pkgver/jql-$pkgver.tar.gz"
options="net" # fetch dependencies

[ "$CARCH" = "riscv64" ] && options="$options textrels"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --frozen --release
}

check() {
	# core::tests::get_property_as_index seems to be broken
	cargo test --frozen -- \
		--skip core::tests::get_property_as_index
}

package() {
	install -D -m755 target/release/jql -t "$pkgdir"/usr/bin/
}

sha512sums="
c7dd9cda1a04d94ff149f2bc2817a9a926cc4d5eb13b667baa59e5a011eb06c9ec7dd304ec2da169065c934452bc087fb69a43cfaa86bf59a1793e99c2435555  jql-5.1.6.tar.gz
"
