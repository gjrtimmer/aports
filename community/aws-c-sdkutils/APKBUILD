# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=aws-c-sdkutils
pkgver=0.1.8
pkgrel=1
pkgdesc="C99 library implementing AWS SDK specific utilities"
url="https://github.com/awslabs/aws-c-sdkutils"
# s390x: aws-c-common
arch="all !s390x"
license="Apache-2.0"
makedepends="
	aws-c-common-dev
	cmake
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-c-sdkutils/archive/refs/tags/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja\
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/aws-c-sdkutils
}

sha512sums="
b634f665ea443a96b4e093fd2c86bff3e453f0450b8f8c6689f097946621b2d6eff9799a90f177c84f034b00a91261c91d74c2a91adbb0dad0e9de275a01fa34  aws-c-sdkutils-0.1.8.tar.gz
"
