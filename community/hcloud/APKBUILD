# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=hcloud
pkgver=1.32.0
pkgrel=1
pkgdesc="Command-line interface for Hetzner Cloud"
url="https://github.com/hetznercloud/cli"
license="MIT"
arch="all"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/hetznercloud/cli/archive/v$pkgver/hcloud-$pkgver.tar.gz"
builddir="$srcdir/cli-$pkgver"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags "-X github.com/hetznercloud/cli/internal/version.Version=$pkgver" \
		-v \
		./cmd/hcloud

	./hcloud completion bash > $pkgname.bash
	./hcloud completion fish > $pkgname.fish
	./hcloud completion zsh > $pkgname.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 hcloud -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
1c1286a2dc79bf557d6e7cfc91400297fe97971611311a449ea5e0c45bab51241a78db9496153815eaa1ec92d89fe2551a9df3356702dbd92683383644f113de  hcloud-1.32.0.tar.gz
"
