# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=containers-common
pkgver=0.51.1
pkgrel=0
pkgdesc="Configuration files for container tools"
url="https://github.com/containers/common"
license="Apache-2.0"
arch="noarch"
options="!check" # no test suite
makedepends="go-md2man"
subpackages="$pkgname-doc"
# Pick the exact versions of common/storage/image vendored in podman.
# Ideally, they should be the same in skopeo and buildah.
# Check them with the list_vendors function. 
_common_ver=$pkgver
_storage_ver=1.45.3
_image_ver=5.24.1
_podman_ver=4.4.4
_skopeo_ver=1.11.1
_buildah_ver=1.29.1
source="https://github.com/containers/common/archive/v$_common_ver/common-$_common_ver.tar.gz
	https://github.com/containers/storage/archive/v$_storage_ver/storage-$_storage_ver.tar.gz
	https://github.com/containers/image/archive/v$_image_ver/image-$_image_ver.tar.gz
	https://github.com/containers/podman/archive/v$_podman_ver/podman-$_podman_ver.tar.gz
	https://github.com/containers/skopeo/archive/v$_skopeo_ver/skopeo-$_skopeo_ver.tar.gz
	https://github.com/containers/buildah/archive/v$_buildah_ver/buildah-$_buildah_ver.tar.gz
	"

list_vendors() {
	unpack

	for tool in podman-$_podman_ver skopeo-$_skopeo_ver buildah-$_buildah_ver
	do
		cd "$srcdir"/$tool
		msg $tool
		grep github.com/containers/common go.mod
		grep github.com/containers/storage go.mod
		grep github.com/containers/image go.mod
	done
}

prepare() {
	default_prepare

	# fix go-md2man path in containers/storage
	sed -E 's/(GOMD2MAN =).*/\1 go-md2man/' -i "$srcdir"/storage-$_storage_ver/docs/Makefile

	# set default storage driver
	sed -E 's/(driver =) ""/\1 "overlay"/' -i "$srcdir"/storage-$_storage_ver/storage.conf

	# set unqualified-search-registries
	sed -E 's/# (unqualified-search-registries =).*/\1 ["docker.io"]/' -i "$srcdir"/image-$_image_ver/registries.conf
}

build() {
	cd "$srcdir"/common-$_common_ver
	make -C docs

	cd "$srcdir"/storage-$_storage_ver
	make -C docs

	cd "$srcdir"/image-$_image_ver
	make docs
}

package() {
	install -d "$pkgdir"/etc/containers/certs.d
	install -d "$pkgdir"/etc/containers/oci/hooks.d
	install -d "$pkgdir"/var/lib/containers/sigstore

	cd "$srcdir"/common-$_common_ver
	install -Dm644 pkg/config/containers.conf "$pkgdir"/etc/containers/containers.conf
	install -Dm644 pkg/config/containers.conf "$pkgdir"/usr/share/containers/containers.conf
	install -Dm644 pkg/seccomp/seccomp.json "$pkgdir"/etc/containers/seccomp.json
	install -Dm644 pkg/seccomp/seccomp.json "$pkgdir"/usr/share/containers/seccomp.json
	make -C docs install PREFIX=/usr DESTDIR="$pkgdir"

	cd "$srcdir"/storage-$_storage_ver
	install -Dm644 storage.conf "$pkgdir"/etc/containers/storage.conf
	install -Dm644 storage.conf "$pkgdir"/usr/share/containers/storage.conf
	make -C docs install DESTDIR="$pkgdir"

	cd "$srcdir"/image-$_image_ver
	install -Dm644 registries.conf "$pkgdir"/etc/containers/registries.conf
	make install DESTDIR="$pkgdir"

	cd "$srcdir"/skopeo-$_skopeo_ver
	install -Dm644 default-policy.json "$pkgdir"/etc/containers/policy.json
	install -Dm644 default.yaml "$pkgdir"/etc/containers/registries.d/default.yaml
}

doc() {
	default_doc
	pkgdesc="Man pages for container tools"
}

sha512sums="
79dbfffba0c6fe53ec794ba33125d2cdded39456a5642b077dbead94d62ca749c7fa1ee71942174b130a5b12ef4f336d9a12bac72c2d883788a67d907f654e41  common-0.51.1.tar.gz
f70524c4604b071c67c8f6845b06d33fdda681f5d4f3b46ce93dfd16f067f1213e55901cfaed58877d32ffd84eeb2e7a49a73836774912b103d457e8c4928cac  storage-1.45.3.tar.gz
acbed3f99a17e6a42a3b4e4d10eccb7bafa3e6e774ec75d48e7dd98210895d55c3cd95a5a8c21e45f900aa8cc9c11d9de8cb023c54768557fb05d7877b608b8f  image-5.24.1.tar.gz
eec7e4630a7f6ee29bda5d6daf302318381d5a1158830bcc266356a3dfbb9eb97b3201e146b4a0c0577dc5633ac74713716c002412317908298b92aea37e11fd  podman-4.4.4.tar.gz
521a2c8ddf629e361340a51d95200ec67bc4fef814e0449b6d679725536ec9448827229d4f993276f084269c2ec73c1b4edf97c0ce29122d950d350ed623a4b9  skopeo-1.11.1.tar.gz
ea78aacee9b5cc10e299789d739e7086579e67719ab72b84f6ea5acbaf31d443284387ac92e7c5c1c3da9bd6523f43e2d9c7e2ea5698c251cb5458a384f41b04  buildah-1.29.1.tar.gz
"
