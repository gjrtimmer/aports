# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-remote-desktop
pkgver=44.0
pkgrel=0
pkgdesc="GNOME Remote Desktop - remote desktop server"
url="https://gitlab.gnome.org/GNOME/gnome-remote-desktop"
arch="all !s390x" # blocked by pipewire
license="GPL-2.0-or-later"
# mesa and libgudev are checkdepends, but they don't get installed
# due to options="!check"
makedepends="
	asciidoc
	cairo-dev
	fdk-aac-dev
	ffnvcodec-headers
	freerdp-dev
	fuse3-dev
	glib-dev
	libdrm-dev
	libepoxy-dev
	libgudev-dev
	libnotify-dev
	libsecret-dev
	libvncserver-dev
	libxkbcommon-dev
	mesa-dev
	meson
	pipewire-dev
	tpm2-tss-dev
	"
checkdepends="
	adwaita-icon-theme
	bash
	dbus
	gnome-settings-daemon
	mutter
	py3-dbus
	py3-gobject3
	xvfb-run
	"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Needs a full fat GNOME session
source="https://download.gnome.org/sources/gnome-remote-desktop/${pkgver%.*}/gnome-remote-desktop-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dsystemd=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	mkdir -p /tmp/runtimedir
	GSETTINGS_SCHEMA_DIR=output/src/ XDG_RUNTIME_DIR=/tmp/runtimedir meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
6ca62126790be4498806aeb61c02edf68f113aa1ec53ab73f8bd822b10f5957b32a67d5a23f7ba023db117a8b2fe8fb3be87ea72bca1c22dedde00e94677549e  gnome-remote-desktop-44.0.tar.xz
"
