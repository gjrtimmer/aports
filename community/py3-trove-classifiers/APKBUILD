# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-trove-classifiers
pkgver=2023.3.9
pkgrel=0
pkgdesc="Canonical source for classifiers on PyPI"
url="https://github.com/pypa/trove-classifiers"
license="Apache-2.0"
arch="noarch"
depends="python3"
makedepends="py3-calver py3-gpep517 py3-installer py3-setuptools py3-wheel"
checkdepends="py3-pytest"
source="https://github.com/pypa/trove-classifiers/archive/$pkgver/py3-trove-classifiers-$pkgver.tar.gz"
builddir="$srcdir/trove-classifiers-$pkgver"

prepare() {
	default_prepare

	echo "Version: $pkgver" > PKG-INFO
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/trove_classifiers-$pkgver-py3-none-any.whl
}

sha512sums="
b7bfff714201ccb84c4d9fcb30509ace90bf336f4c6432bf64076c59c9cb9027a6c66ae6ce815eb2cfaa32215735926710ef2710f97ba031d3fdd073c1854c54  py3-trove-classifiers-2023.3.9.tar.gz
"
