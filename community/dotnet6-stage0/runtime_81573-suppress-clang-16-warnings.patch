From 9e1945b03c7dd7772f2b97a1149f07f7cbd63ed8 Mon Sep 17 00:00:00 2001
Patch-Source: https://github.com/dotnet/runtime/pull/81573
From: Antoine Martin <dev@ayakael.net>
Date: Tue, 28 Mar 2023 13:58:40 -0400
Subject: [PATCH 1/1] Suppress clang-16 warnings

---
 eng/native/configurecompiler.cmake                |  8 ++++++++
 src/coreclr/dlls/mscordbi/CMakeLists.txt          |  5 +----
 src/coreclr/dlls/mscordbi/mscordbi.cpp            | 15 +++++++++++++++
 src/native/corehost/apphost/static/CMakeLists.txt |  4 ++--
 .../apphost/static/singlefilehost_OSXexports.src  | 11 -----------
 .../apphost/static/singlefilehost_unixexports.src |  4 ----
 6 files changed, 26 insertions(+), 21 deletions(-)
 delete mode 100644 src/native/corehost/apphost/static/singlefilehost_OSXexports.src

diff --git a/src/runtime/eng/native/init-compiler.sh b/src/runtime/eng/native/init-compiler.sh
index 883c7c5..4555cbe 100755
--- a/src/runtime/eng/native/init-compiler.sh
+++ b/src/runtime/eng/native/init-compiler.sh
@@ -46,8 +46,8 @@ if [[ -z "$CLR_CC" ]]; then
     # Set default versions
     if [[ -z "$majorVersion" ]]; then
         # note: gcc (all versions) and clang versions higher than 6 do not have minor version in file name, if it is zero.
-        if [[ "$compiler" == "clang" ]]; then versions=( 11 10 9 8 7 6.0 5.0 4.0 3.9 3.8 3.7 3.6 3.5 )
-        elif [[ "$compiler" == "gcc" ]]; then versions=( 11 10 9 8 7 6 5 4.9 ); fi
+        if [[ "$compiler" == "clang" ]]; then versions=( 16 15 14 13 12 11 10 9 8 7 6.0 5.0 4.0 3.9 3.8 3.7 3.6 3.5 )
+        elif [[ "$compiler" == "gcc" ]]; then versions=( 12 11 10 9 8 7 6 5 4.9 ); fi
 
         for version in "${versions[@]}"; do
             parts=(${version//./ })
diff --git a/src/runtime/eng/native/configurecompiler.cmake b/src/runtime/eng/native/configurecompiler.cmake
index f3526deb19f..6cf0577a774 100644
--- a/src/runtime/eng/native/configurecompiler.cmake
+++ b/src/runtime/eng/native/configurecompiler.cmake
@@ -367,6 +367,15 @@ if (CLR_CMAKE_HOST_UNIX)
     add_compile_options(-Wno-incompatible-ms-struct)
 
     add_compile_options(-Wno-reserved-identifier)
+
+    # clang 16.0 introduced buffer hardening https://discourse.llvm.org/t/rfc-c-buffer-hardening/65734
+    # which we are not conforming to yet.
+    add_compile_options(-Wno-unsafe-buffer-usage)
+
+    # other clang 16.0 suppressions
+    add_compile_options(-Wno-single-bit-bitfield-constant-conversion)
+    add_compile_options(-Wno-cast-function-type-strict)
+    add_compile_options(-Wno-incompatible-function-pointer-types-strict)
   else()
     add_compile_options(-Wno-unknown-pragmas)
     add_compile_options(-Wno-uninitialized)
diff --git a/src/runtime/src/coreclr/dlls/mscordbi/CMakeLists.txt b/src/runtime/src/coreclr/dlls/mscordbi/CMakeLists.txt
index c7a23c9923f..4a03a788a71 100644
--- a/src/runtime/src/coreclr/dlls/mscordbi/CMakeLists.txt
+++ b/src/runtime/src/coreclr/dlls/mscordbi/CMakeLists.txt
@@ -99,10 +99,7 @@ elseif(CLR_CMAKE_HOST_UNIX)
         mscordaccore
     )
 
-    # COREDBI_LIBRARIES is mentioned twice because ld is one pass linker and will not find symbols
-    # if they are defined after they are used. Having all libs twice makes sure that ld will actually
-    # find all symbols.
-    target_link_libraries(mscordbi ${COREDBI_LIBRARIES} ${COREDBI_LIBRARIES})
+    target_link_libraries(mscordbi ${COREDBI_LIBRARIES})
 
     add_dependencies(mscordbi mscordaccore)
 
diff --git a/src/runtime/src/coreclr/dlls/mscordbi/mscordbi.cpp b/src/runtime/src/coreclr/dlls/mscordbi/mscordbi.cpp
index afd2cfe8002..489c552a0bd 100644
--- a/src/runtime/src/coreclr/dlls/mscordbi/mscordbi.cpp
+++ b/src/runtime/src/coreclr/dlls/mscordbi/mscordbi.cpp
@@ -26,3 +26,18 @@ BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
 	// Defer to the main debugging code.
     return DbgDllMain(hInstance, dwReason, lpReserved);
 }
+
+#if defined(HOST_LINUX) && defined(TARGET_LINUX)
+PALIMPORT HINSTANCE PALAPI DAC_PAL_RegisterModule(IN LPCSTR lpLibFileName);
+PALIMPORT VOID PALAPI DAC_PAL_UnregisterModule(IN HINSTANCE hInstance);
+
+HINSTANCE PALAPI PAL_RegisterModule(IN LPCSTR lpLibFileName)
+{
+     return DAC_PAL_RegisterModule(lpLibFileName);
+}
+
+VOID PALAPI PAL_UnregisterModule(IN HINSTANCE hInstance)
+{
+    DAC_PAL_UnregisterModule(hInstance);
+}
+#endif
diff --git a/src/runtime/src/native/corehost/apphost/static/CMakeLists.txt b/src/runtime/src/native/corehost/apphost/static/CMakeLists.txt
index a00c7913986..6d491c086da 100644
--- a/src/runtime/src/native/corehost/apphost/static/CMakeLists.txt
+++ b/src/runtime/src/native/corehost/apphost/static/CMakeLists.txt
@@ -73,8 +73,8 @@ if(CLR_CMAKE_TARGET_WIN32)
     add_linker_flag("/DEF:${CMAKE_CURRENT_SOURCE_DIR}/singlefilehost.def")
 
 else()
-    if(CLR_CMAKE_TARGET_OSX)
-        set(DEF_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/singlefilehost_OSXexports.src)
+    if(CLR_CMAKE_TARGET_FREEBSD)
+        set(DEF_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/singlefilehost_freebsdexports.src)
     else()
         set(DEF_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/singlefilehost_unixexports.src)
     endif()
diff --git a/src/runtime/src/native/corehost/apphost/static/singlefilehost_OSXexports.src b/src/runtime/src/native/corehost/apphost/static/singlefilehost_OSXexports.src
deleted file mode 100644
index 18d5697e845..00000000000
--- a/src/runtime/src/native/corehost/apphost/static/singlefilehost_OSXexports.src
+++ /dev/null
@@ -1,11 +0,0 @@
-; Licensed to the .NET Foundation under one or more agreements.
-; The .NET Foundation licenses this file to you under the MIT license.
-
-; needed by SOS
-DotNetRuntimeInfo
-
-; DAC table export
-g_dacTable
-
-; Used by profilers
-MetaDataGetDispenser
diff --git a/src/runtime/src/native/corehost/apphost/static/singlefilehost_unixexports.src b/src/runtime/src/native/corehost/apphost/static/singlefilehost_unixexports.src
index 1f9c5178218..18d5697e845 100644
--- a/src/runtime/src/native/corehost/apphost/static/singlefilehost_unixexports.src
+++ b/src/runtime/src/native/corehost/apphost/static/singlefilehost_unixexports.src
@@ -9,7 +9,3 @@ g_dacTable
 
 ; Used by profilers
 MetaDataGetDispenser
-
-; FreeBSD needs to reexport these
-__progname
-environ 
-- 
diff --git a/src/runtime/src/libraries/Native/Unix/CMakeLists.txt b/src/runtime/src/libraries/Native/Unix/CMakeLists.txt
index 6931f62d24c..4f7375a59c6 100644
--- a/src/runtime/src/libraries/Native/Unix/CMakeLists.txt
+++ b/src/runtime/src/libraries/Native/Unix/CMakeLists.txt
@@ -51,6 +51,15 @@ if(CMAKE_C_COMPILER_ID STREQUAL Clang)
     add_compile_options(-Wthread-safety)
     add_compile_options(-Wno-thread-safety-analysis)
     add_compile_options(-Wno-reserved-identifier)
+
+	# clang 16.0 introduced buffer hardening https://discourse.llvm.org/t/rfc-c-buffer-hardening/65734
+    # which we are not conforming to yet.
+    add_compile_options(-Wno-unsafe-buffer-usage)
+
+    # other clang 16.0 suppressions
+    add_compile_options(-Wno-single-bit-bitfield-constant-conversion)
+    add_compile_options(-Wno-cast-function-type-strict)
+	 add_compile_options(-Wno-incompatible-function-pointer-types-strict)
 elseif(CMAKE_C_COMPILER_ID STREQUAL GNU)
     add_compile_options(-Wno-stringop-truncation)
 endif()
2.38.4

