# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-awscrt
pkgver=0.16.13
pkgrel=0
pkgdesc="Python bindings for the AWS Common Runtime"
url="https://github.com/awslabs/aws-crt-python"
# s390x: big endian is explicitly unsupported
# arm*, ppc64le: aws-crt-cpp
arch="all !armhf !armv7 !ppc64le !s390x"
license="Apache-2.0"
# use the cpp one to just pull the aws stack
makedepends="
	aws-crt-cpp-dev
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	samurai
	"
checkdepends="py3-websockets"
source="$pkgname-$pkgver-2.tar.gz::https://github.com/awslabs/aws-crt-python/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/aws-crt-python-$pkgver"
options="net" # tests need internet

case "$CARCH" in
arm*|aarch64|ppc64le)
	# too slow at running tests / fix later
	options="$options !check"
	;;
esac
prepare() {
	default_prepare

	# by default it's just 1.0.0.dev0
	echo "__version__ = '$pkgver'" >> awscrt/__init__.py

	# dynlink
	sed -i '/:lib/d' setup.py
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m unittest discover test
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
f216de27f55007de68cc994d3913cf69340e049e957b5e0ab00f31df8b7d62f112b253470a0d7b23dc6749f46d48349f13e8fbef4cb2aefd5952742656f4c33e  py3-awscrt-0.16.13-2.tar.gz
"
