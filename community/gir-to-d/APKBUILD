# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gir-to-d
pkgver=0.23.0
pkgrel=1
pkgdesc="Create D bindings from GObject introspection files"
url="https://github.com/gtkd-developers/gir-to-d"
# limited by ldc
arch="aarch64 x86_64"
license="LGPL-3.0-or-later"
makedepends="meson ldc"
options="!check" # no tests
source="https://github.com/gtkd-developers/gir-to-d/archive/v$pkgver/gir-to-d-$pkgver.tar.gz
	$pkgname-glib2.76.patch::https://github.com/gtkd-developers/gir-to-d/commit/7a7e5845dc74ac1ef845770e629730560ee3b69a.patch
	"

build() {
	# Pass --allinst to ldc to work around an undefined reference build issue
	# https://github.com/ldc-developers/ldc/issues/4000
	LDFLAGS= abuild-meson -Dd_args="--allinst" . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
d481bf9734c7362122af4ef82db9ff06876c1172f7de8bd9d4505be4ddd47452e01e3e4ecdefa0f9158ab720cb69f1a5d8e5e120c3a50b4d962a8fd6b80afdc7  gir-to-d-0.23.0.tar.gz
bff351dba2ad2568ec014fe6ca0cf4bb04e3fcdacd228a3402bdd3679a4310d634ab50d6c2b7cb032316da7e484fa6d6953a7870257b8860861e8f2b291af941  gir-to-d-glib2.76.patch
"
