# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=coreutils
pkgver=9.2
pkgrel=3
pkgdesc="The basic file, shell and text manipulation utilities"
url="https://www.gnu.org/software/coreutils/"
arch="all"
license="GPL-3.0-or-later"
makedepends="acl-dev attr-dev utmps-dev perl"
subpackages="$pkgname-doc"
install="$pkgname.post-deinstall"
source="https://ftp.gnu.org/gnu/coreutils/coreutils-$pkgver.tar.xz
	fix-reported-checksum-status.patch
	fix-copy-restricted.patch
	"
options="!check"

# secfixes:
#   8.30-r0:
#     - CVE-2017-18018

build() {
	CFLAGS="$CFLAGS -I/usr/include/utmps -flto=auto" \
	LIBS="-lutmps -lskarnet" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-nls \
		--enable-no-install-program=hostname,su,kill,uptime \
		--enable-single-binary=symlinks
	make
}

package() {
	make DESTDIR="$pkgdir" install

	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true

	install -d "$pkgdir"/bin "$pkgdir"/usr/sbin
	cd "$pkgdir"/usr/bin/

	# binaries that busybox puts in /bin
	local busybox_bin="base64 cat chgrp chmod chown cp date dd df echo false ln ls
		mkdir mknod mktemp mv nice printenv pwd rm rmdir sleep stat stty sync touch true uname"

	# as these binaries live in /bin on busybox, we want to put them in /bin with coreutils
	for i in $busybox_bin; do
		rm "$pkgdir"/usr/bin/$i
		ln -s ../usr/bin/coreutils "$pkgdir"/bin/$i
	done

	# chroot lives in /usr/sbin with busybox
	rm "$pkgdir"/usr/bin/chroot
	ln -s ../bin/coreutils "$pkgdir"/usr/sbin/chroot

	# resolve conflict between shadow and coreutils for cmd:groups
	rm "$pkgdir"/usr/bin/groups
}

# XXX - some gnulib tests broken, find a way to patch out gnulib tests
check() {
	make check
}

sha512sums="
7e3108fefba4ef995cc73c64ac5f4e09827a44649a97ddd624eb61d67ce82da5ed6dc8c0f79d3e269f5cdb7d43877a61ef5b93194dd905bec432a7e31f9f479c  coreutils-9.2.tar.xz
2c9072d8620eda2740992abe5482af918193bb9bfdfe634b049b45e3c6b5f04ef90aefb22d5ae795fb03d694eb582a8684c03e80e6212b7d62b6f84203e84a9e  fix-reported-checksum-status.patch
325df63490b3c91fe6e7795aa9866f86f23cf3484cb9e2275738ec5e3c187ed72e77b63e3eec5830ffb9ca05164ce4540edd6bbbe7faec29426e47dd2343d9ce  fix-copy-restricted.patch
"
