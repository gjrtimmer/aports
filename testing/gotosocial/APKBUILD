# Contributor: Alex McGrath <amk@amk.ie>
# Maintainer: Alex McGrath <amk@amk.ie>
pkgname=gotosocial
pkgver=0.8.0
pkgrel=0
pkgdesc="An ActivityPub social network server"
url="https://github.com/superseriousbusiness/gotosocial"
arch="all"
license="AGPL-3.0-only"
install="$pkgname.pre-install"
pkgusers="gotosocial"
pkggroups="gotosocial"
makedepends="go yarn nodejs"
subpackages="$pkgname-openrc"
source="https://github.com/superseriousbusiness/gotosocial/releases/download/v$pkgver/gotosocial-$pkgver-source-code.tar.gz
	gotosocial.initd
	"
builddir="$srcdir"

case "$CARCH" in
	# flaky test
	s390x) options="!check";;
esac

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	yarn install --cwd web/source
	BUDO_BUILD=1 node web/source
	go build -o gotosocial ./cmd/gotosocial
}

check() {
	# skip tests in internal/media as they're failing
	local pkgs="$(go list ./... | grep -v 'internal/media')"
	CGO_ENABLED=0 GTS_DB_TYPE="sqlite" GTS_DB_ADDRESS=":memory:" \
		go test $pkgs
}

package() {
	install -Dm755 "$srcdir"/gotosocial.initd \
		"$pkgdir"/etc/init.d/gotosocial
	install -Dm755 "$builddir"/gotosocial \
		-t "$pkgdir"/usr/bin/

	install -d -o gotosocial -g gotosocial "$pkgdir"/etc/gotosocial
	install -Dm640 "$builddir"/example/config.yaml "$pkgdir"/etc/gotosocial/gotosocial.yml

	mkdir -p "$pkgdir"/usr/share/webapps/gotosocial
	cp -r "$builddir"/web/assets "$pkgdir"/usr/share/webapps/gotosocial
	cp -r "$builddir"/web/template "$pkgdir"/usr/share/webapps/gotosocial

	install -d -o gotosocial -g gotosocial "$pkgdir"/var/lib/gotosocial
}

sha512sums="
b3bbb160e039ddc8e36165bd39e6d633d719d6bed49a9c8a3e0a2bd8d272c42940132126b076a8dd8998cf2497c888f033baf6f23b44901c3f270efa87db6ba4  gotosocial-0.8.0-source-code.tar.gz
f9e4aa4177d727c5a2822e292a54b1d488c60c1f2f62fc64d193e3de6c1168ae74c6273eaed86f148cf0390505903bcf19b97f375d7f2838a892275abd79d9d6  gotosocial.initd
"
